from flask import Flask
from flask import render_template
import logging

########################################################################################
# Name: Lee Burndred
# Date: 10/21/19
# Notes: 1. Got a lot of info from flask.palletprojects.com/en/1.1.x/
#        2. Also got help from hackersandslackers.com/the-art-of-building-flask-routes/
#
########################################################################################



# want a way to debug, emulated logging from project1 pageserver.py source file
logging.basicConfig(format='(levelname)s:%(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)


app = Flask(__name__)


@app.route("/")
def hello():
    return render_template("index.html")

@app.route("/<path_name>")
def test_logic(path_name):
    print(path_name)
    if "//" in path_name or "~" in path_name or ".." in path_name:
        return render_template("403.html"), 403
    else:
        try:
            return render_template(path_name)
        except Exception as e:
            return render_template("404.html"), 404

@app.route("/<path_name0>/<path_name1>")
def longer_path_logic(path_name0, path_name1):
    print(path_name0)
    print(path_name1)
    if "//" in path_name0 or "~" in path_name0 or "~" in path_name1 or ".." in path_name0 or ".." in path_name1 or "//" in path_name1:
        return render_template("403.html"), 403
    else:
        try:
            return render_template(path_name0 + path_name1)
        except Exception as e:
            return render_template("404.html"), 404

@app.route("/<path_name0>//<path_name1>")
def bad_long_path(path_name0, path_name1):
    return render_template("403.html"), 403

@app.errorhandler(404)
def not_found_error(error):
    return render_template("404.html"), 404

@app.errorhandler(403)
def forbidden_error(error):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
